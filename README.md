# Tanga
Tanga is a WebApp built with ReactJs and NodeJs. 
It provides a route optimization service with Graphhopper for carriers which use sustainable vehicles.

You need an API key from [Graphhopper](https://www.graphhopper.com/) and [Google Maps](https://developers.google.com/maps/documentation/javascript/get-api-key).
You can add them as ENV vars (see server/config.js).

## How to build
If you would like to run the app on your own computer you need to have [Node.js and NPM](https://nodejs.org/en/) installed.

After cloning the repository navigate into the *client-folder* (`cd client`) and run `npm install` from your console. Then navigate into your *server-folder* (`cd ..` and `cd server`) and run `npm install` again.

In your *root folder* (`cd ..`) run the app with `npm start`. You can access the website at <http://127.0.0.1:3001/>.