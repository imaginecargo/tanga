import React, { Component } from 'react';
import {Icon, Table} from "semantic-ui-react";
import MapboxAutocomplete from 'react-mapbox-autocomplete';

import style from './index.module.css';
import TimePicker from "rc-time-picker/lib/TimePicker";

const LOCAL_STORAGE_KEY = 'tanga_vehicles';

const DEFAULT_START_TIME = ''

class VehicleForm extends Component {

  constructor(props) {
    super(props);
    this.state =  {
      type_id: '',
      name: 'eVan',
      profile: 'car',
      speed_factor: 1,
      number: 1,
      capacity: [100000],
      street: "Hohlstr.",
      street_number: "400",
      place: "Zürich",
      country: "Switzerland",
      vehicle_types: [],
      costs: 0,
      earliest_start: 0,
      latest_end: 0
    };

    try {
      const localData = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
      if (Array.isArray(localData)) {
        this.state.vehicle_types = localData;
        this.props.sendVehicleTypes(this.state.vehicle_types);
      }
    } catch (e) {
      console.warn('could not read vehicles form localstorage');
      localStorage.removeItem(LOCAL_STORAGE_KEY);
    }

    this.handleValueOfDropDown = this.handleValueOfDropDown.bind(this);
    this.handleChanges = this.handleChanges.bind(this);
    this.handleChangesAddress = this.handleChangesAddress.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDeletion = this.handleDeletion.bind(this);

  }

  handleValueOfDropDown(event){
    switch (event.target.value) {
      case 'eVan':
        this.setState({name: 'eVan', profile: 'car', speed_factor: 1});
        break;
      case 'trike':
        this.setState({name: 'Trike', profile: 'car', speed_factor: 0.7});
        break;
      case 'bike':
        this.setState({name: 'Bike', profile: 'bike', speed_factor: 1});
        break;
      default:
        this.setState({name: 'Trike', profile: 'car', speed_factor: 0.7});
    }

  }

  handleChanges(event) {
    const target = event.target;
    let value = target.value;
    if(target.name === "capacity"){
      value = [parseInt(target.value, 10)*1000]
    }

    const name = target.name;


    this.setState({
      [name]: value
    });
  }

  handleTimeChanges(target, value) {
    this.setState({
      [target]: value.toISOString()
    })
  }

  handleChangesAddress(address) {
    this.setState({'address': address});
  }

  handleSubmit(event){
    event.preventDefault();
    let currentVehicleTypes = this.state.vehicle_types;
    currentVehicleTypes.push(
      {
        type_id: this.state.vehicle_types.length +1  ,
        profile: this.state.profile,
        capacity: this.state.capacity,
        speed_factor: this.state.speed_factor,
        earliest_start: this.state.earliest_start,
        latest_end: this.state.latest_end,
        options: {
          number: this.state.number,
          name: this.state.name,
          costs: this.state.costs,
          depot: {
            address: this.state.address
          }
        }
      });

      this.setState({
        vehicle_types: currentVehicleTypes
      });
    this.props.sendVehicleTypes(this.state.vehicle_types);
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(this.state.vehicle_types))
  }

  handleDeletion(index){
    const vehicle_types = this.state.vehicle_types.filter((e, i) => i !== index);
    this.setState({
      vehicle_types,
    });
    this.props.sendVehicleTypes(vehicle_types);
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(vehicle_types));
  }


  render() {

    return (
      <div className={style.root}>
        <Table celled striped compact>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Amount</Table.HeaderCell>
              <Table.HeaderCell>Type</Table.HeaderCell>
              <Table.HeaderCell>Capacity</Table.HeaderCell>
              <Table.HeaderCell>Costs</Table.HeaderCell>
              <Table.HeaderCell>Av. From</Table.HeaderCell>
              <Table.HeaderCell>Av. To</Table.HeaderCell>
              <Table.HeaderCell>Address</Table.HeaderCell>
              <Table.HeaderCell>Delete</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            { this.state.vehicle_types.map((vehicle, i) => {
                return (
                    <Table.Row key={i}>
                      <Table.Cell>
                        {vehicle.options.number}
                      </Table.Cell>
                      <Table.Cell>
                        {vehicle.options.name}
                      </Table.Cell>
                      <Table.Cell>
                        {vehicle.capacity}
                      </Table.Cell>
                      <Table.Cell>
                        {vehicle.options.costs}
                      </Table.Cell>
                      <Table.Cell>
                        {vehicle.earliest_start}
                      </Table.Cell>
                      <Table.Cell>
                        {vehicle.latest_end}
                      </Table.Cell>
                      <Table.Cell>
                        {vehicle.options.depot.address}
                      </Table.Cell>
                      <Table.Cell>
                        <Icon name='delete' onClick={() => this.handleDeletion(i)} />
                      </Table.Cell>
                    </Table.Row>
                    )
              })
            }
            <Table.Row>
              <Table.Cell>
                <input type="number" pattern="[0-9]*" name="number" className="u-full-width" placeholder={"Anzahl"} onChange={this.handleChanges}/>
              </Table.Cell>
              <Table.Cell>
                <select className="u-full-width"  onChange={this.handleValueOfDropDown}>
                  <option value='eVan'>eVan</option>
                  <option value='trike'>Trike</option>
                  <option value='bike'>Bike</option>
                </select>
              </Table.Cell>
              <Table.Cell>
                <input type="number" pattern="[0-9]*" name="capacity" className="u-full-width" placeholder={"Volumengewicht"} onChange={this.handleChanges}/>
              </Table.Cell>
              <Table.Cell>
                <input type="number" pattern="[0-9]*" name="costs" className="u-full-width" placeholder={"Kosten"} onChange={this.handleChanges}/>
              </Table.Cell>
              <Table.Cell>
                <TimePicker name="earliestStart" showSecond={false}  className={style.timePickerLeft} onChange={this.handleTimeChanges.bind(this, 'earliest_start')} placeholder="Vefügbar ab"/>
              </Table.Cell>
              <Table.Cell>
                <TimePicker name="latestEnd" showSecond={false}  className={style.timePickerLeft} onChange={this.handleTimeChanges.bind(this, 'latest_end')} placeholder="Vefügbar bis"/>
              </Table.Cell>
              <Table.Cell>
                <MapboxAutocomplete publicKey='pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA'
                                    name='address'
                                    inputClass=''
                                    onSuggestionSelect={this.handleChangesAddress}
                                    country='ch,de'
                                    resetSearch={false}/>
              </Table.Cell>
              <Table.Cell>
                <button className={style.submit} onClick={this.handleSubmit}>Add Vehicle</button>
              </Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
      </div>
    );
  }
}

export default VehicleForm;