import 'rc-time-picker/assets/index.css';
import React, { Component } from 'react';
import TimePicker from 'rc-time-picker';
import moment from 'moment';

import style from './index.module.css'

import FileInput from '../../components/FileInput'


const defaultValues = {
  earliest_pickup: '',
  latest_pickup: '',
  earliest_delivery: '',
  latest_delivery: '',
  pickup_duration: 0,
  delivery_duration: 0,
};


class CreateStopps extends Component {

  constructor(props) {
    super(props);
    this.state =  {
      indexOfStop: null,
      isModified: false,
      pickupStreet: '',
      pickupNumber: '',
      pickupPlace: '',
      pickupCountry: '',
      deliveryStreet: '',
      deliveryNumber: '',
      deliveryPlace: '',
      deliveryCountry: '',
      stopData: [],
      earliestPickup: null,
      latestPickup: null,
      earliestDelivery: null,
      latestDelivery: null,
      pickupDuration: '',
      deliveryDuration: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleDeletion = this.handleDeletion.bind(this);
    this.getJSON = this.getJSON.bind(this);
    this.handleDate = this.handleDate.bind(this);
    this.modifyStop = this.modifyStop.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.initialStateForForm = this.initialStateForForm.bind(this);
  }

  initialStateForForm(){
    this.setState({
      isModified: false,
      pickupStreet: '',
      pickupNumber: '',
      pickupPlace: '',
      pickupCountry: '',
      deliveryStreet: '',
      deliveryNumber: '',
      deliveryPlace: '',
      deliveryCountry: '',
      earliestPickup: null,
      latestPickup: null,
      earliestDelivery: null,
      latestDelivery: null,
      pickupDuration: '',
      deliveryDuration: '',
    })
  }

  handleDeletion(){
    let currentAddress = this.state.stopData;
    if(this.state.isModified === true){
      currentAddress.splice(this.state.indexOfStop, 1)
    }
    else{
      currentAddress.pop();
    }
    this.setState({
      stopData: currentAddress
    });
    this.initialStateForForm();
    this.props.sendData(this.state.stopData)
  }

  handleDate(target, value){
    this.setState({
      [target]: value

    })

  }

  handleChange(event){
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  getJSON(obj){
    obj.forEach(stopp => {
      const object = Object.assign({}, defaultValues, stopp);
      this.state.stopData.push(object);

    });
    this.props.sendData(this.state.stopData)

  }

  modifyStop(stop, i){

    const earliestPickup = moment(stop.earliest_pickup);
    const latestPickup = moment(stop.latest_pickup);
    const earliestDelivery = moment(stop.earliest_delivery);
    const latestDelivery = moment(stop.latest_delivery);

    this.setState({
      isModified: true,
      indexOfStop: i,
      pickupStreet: stop.pickup_street,
      pickupNumber: stop.pickup_number,
      pickupPlace: stop.pickup_place,
      pickupCountry: stop.pickup_country,
      pickupDuration: stop.pickup_duration / 60,
      earliestPickup: earliestPickup,
      latestPickup: latestPickup,
      deliveryStreet: stop.delivery_street,
      deliveryNumber: stop.delivery_number,
      deliveryPlace: stop.delivery_place,
      deliveryCountry: stop.delivery_country,
      deliveryDuration: stop.delivery_duration / 60,
      earliestDelivery: earliestDelivery,
      latestDelivery: latestDelivery

    })
  }

  handleSubmit(){
    let stops = this.state.stopData;
    const currentAddress = {
      pickup_street: this.state.pickupStreet,
      pickup_number: this.state.pickupNumber,
      pickup_place: this.state.pickupPlace,
      pickup_country: this.state.pickupCountry,
      delivery_street: this.state.deliveryStreet,
      delivery_number: this.state.deliveryNumber,
      delivery_place: this.state.deliveryPlace,
      delivery_country: this.state.deliveryCountry,
      earliest_pickup: this.state.earliestPickup,
      latest_pickup: this.state.latestPickup,
      earliest_delivery: this.state.earliestDelivery,
      latest_delivery: this.state.latestDelivery,
      pickup_duration: this.state.pickupDuration * 60,
      delivery_duration: this.state.deliveryDuration * 60
    };
    if(this.state.isModified === true){
      stops[this.state.indexOfStop] = currentAddress;
    }
    else{
      stops.push(currentAddress)
    }

    this.setState({
      stopData: stops,
    });
    this.initialStateForForm();
    this.props.sendData(this.state.stopData)
  }

  render() {
    let addressList = this.state.stopData.map((address, i) => {
      return <div key={i} onClick={() => this.modifyStop(address, i)}>
        <span className={style.address}><b>Abholadresse: </b>{`${address.pickup_street} ${address.pickup_number}, ${address.pickup_place}`}</span>
        <br/>
        <span className={style.address}><b>Lieferadresse: </b>{`${address.delivery_street} ${address.delivery_number}, ${address.delivery_place}`}</span>
        <br/>
        <br/>
      </div>;});

    return (
      <div className={style.root}>
        <div className="container u-full-width u-max-full-width">
          <h4>Stopps erstellen</h4>
          <div className="row">
            <div className="two-thirds column">
              <div className="row">
                <div className="six columns">
                  <div className={style.form}>
                    <input value={this.state.pickupStreet} type="text"  name="pickupStreet" className={style.streetname} placeholder={"Straße Abholung"} onChange={this.handleChange}/>
                    <input value={this.state.pickupNumber} type="text"  name="pickupNumber" className={style.number} placeholder={"Nr."} onChange={this.handleChange}/>
                    <input value={this.state.pickupPlace} type="text"  name="pickupPlace" className={style.place} placeholder={"Ort Abholung"} onChange={this.handleChange}/>
                    <input value={this.state.pickupCountry} type="text"  name="pickupCountry" className={style.place} placeholder={"Land Abholung"} onChange={this.handleChange}/>
                    <input value={this.state.pickupDuration} type="number" name="pickupDuration" className={style.place} placeholder={"Dauer der Abholung in Minuten"} onChange={this.handleChange}/>
                    <TimePicker value={this.state.earliestPickup} showSecond={false}  className={style.timePickerLeft} onChange={this.handleDate.bind(this, 'earliestPickup')} placeholder="Früheste Abholung"/>
                    <TimePicker value={this.state.latestPickup} showSecond={false}  className={style.timePickerRight} onChange={this.handleDate.bind(this, 'latestPickup')} placeholder="Späteste Abholung"/>
                  </div>
                </div>
                <div className="six columns">
                  <div className={style.form}>
                    <input value={this.state.deliveryStreet} type="text"  name="deliveryStreet" className={style.streetname} placeholder={"Straße Lieferung"} onChange={this.handleChange}/>
                    <input value={this.state.deliveryNumber} type="text"  name="deliveryNumber" className={style.number} placeholder={"Nr."} onChange={this.handleChange}/>
                    <input value={this.state.deliveryPlace} type="text"  name="deliveryPlace" className={style.place} placeholder={"Ort Lieferung"} onChange={this.handleChange}/>
                    <input value={this.state.deliveryCountry} type="text"  name="deliveryCountry" className={style.place} placeholder={"Land Lieferung"} onChange={this.handleChange}/>
                    <input value={this.state.deliveryDuration} type="number" name="deliveryDuration" className={style.place} placeholder={"Dauer der Lieferung in Minuten"} onChange={this.handleChange}/>
                    <TimePicker value={this.state.earliestDelivery} showSecond={false}  className={style.timePickerLeft} onChange={this.handleDate.bind(this, 'earliestDelivery')} placeholder="Früheste Lieferung"/>
                    <TimePicker value={this.state.latestDelivery} showSecond={false}  className={style.timePickerRight} onChange={this.handleDate.bind(this, 'latestDelivery')} placeholder="Späteste Lieferung"/>
                  </div>
                </div>
              </div>
              { this.state.isModified !== true &&
                <div>
                  <button className={style.submit} onClick={this.handleSubmit}>Add Stopp</button>
                  <button className={style.delete} onClick={this.handleDeletion}>Delete Stopp</button>
                </div>
              }
              {this.state.isModified === true &&
                <div>
                  <button className={style.submit} onClick={this.handleSubmit}>Modify</button>
                  <button className={style.delete} onClick={this.handleDeletion}>Delete Stopp</button>
                </div>}
            </div>
            <div className="one-third column">
              <div className={style.list}>
                <h6 className={style.headLineList}>Stopps:</h6>
                {addressList}
              </div>
             <FileInput sendData={this.getJSON}/>
            </div>
            </div>
        </div>
      </div>
    );


  }
}

export default CreateStopps;
