import React from 'react';
import Files from "react-files";
import Papa from "papaparse/papaparse.min.js";
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons/faInfoCircle';
import Tooltip from '../../components/Tooltip';

import style from './index.module.css'

library.add(faInfoCircle);

const config = {
  delimiter: "",	// auto-detect
  newline: "",	// auto-detect
  quoteChar: '"',
  escapeChar: '"',
  header: true,
  trimHeaders: false,
  dynamicTyping: false,
  preview: 0,
  encoding: "",
  worker: false,
  comments: false,
  download: false,
  skipEmptyLines: true
};

const PANDA_PROPS_MAP = {
  pickupStreet: 'pickup_street',
  pickupCity: '+pickup_place',
  pickupZip: '+pickup_place',
  pickupCountry: 'pickup_country',
  deliveryStreet: 'delivery_street',
  deliveryCity: '+delivery_place',
  deliveryZip: '+delivery_place',
  deliveryCountry: 'delivery_country',
  pickupTimeFrom: 'earliest_pickup',
  pickupTimeTo: 'latest_pickup',
  deliveryTimeFrom: 'earliest_delivery',
  deliveryTimeTo: 'latest_delivery',
};


class FileInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      input: {},
      showTooltip: false
    };

    this.onClick = this.onClick.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.fileReader = new FileReader();

  }

  mapUploadToCorrectProperties(data) {
    return data.map((stop) => {
      const result = {};
      Object.keys(stop).forEach((prop) => {
        if(prop in PANDA_PROPS_MAP) {
          if(PANDA_PROPS_MAP[prop].startsWith('+')) {
            const tangaProp = PANDA_PROPS_MAP[prop].slice(1)
            if(result[tangaProp] !== undefined) {
              result[tangaProp] += `, ${stop[prop]}`;
            } else {
              result[tangaProp] = stop[prop];
            }
          } else {
            result[PANDA_PROPS_MAP[prop]] = stop[prop]
          }
        }
      });
      result['pickup_number'] = '';
      result['delivery_number'] = '';
      return result;
    })
  }

  handleChange(event){
    this.fileReader.readAsText(event);
    this.fileReader.onload = (event) => {
      try {
        const result = Papa.parse(event.target.result, config);
        if(result.errors.length === 0)
        {
          this.setState({input: result.data}, ()=> {
            this.props.sendData(this.mapUploadToCorrectProperties(this.state.input))
          });
          return
        }
        else throw new Error('Not a CSV file');
      } catch(e) {
        console.warn(e)
      }

      try{
        if(event.target.error === null){
          this.setState({input: JSON.parse(event.target.result)}, () => {
            this.props.sendData(this.state.input)
          })
        }
        else throw new Error('Not a JSON file');
      } catch(e){
        console.warn(e)
      }
    };

  }


  onClick(){
    if(this.state.showTooltip === false){
      this.setState({ showTooltip: true });
    }
    else{
      this.setState({ showTooltip: false });
    }

  }

  render() {
    return (
      <div className="row">
        <div className="two-thirds column">
          <div className="files">
            <Files
              className="files-dropzone"
              onChange={file => this.handleChange(file[0])}
              onError={err => console.log(err)}
              accepts={[".json", ".csv"]}
              multiple
              maxFiles={3}
              maxFileSize={10000000}
              minFileSize={0}
              clickable
            >
              <button>upload File</button>
            </Files>
          </div>
        </div>
        <div className="one-third column">
          <a className={style.infoCircle}> <FontAwesomeIcon icon="info-circle"  onClick={this.onClick}/></a>
          {
            this.state.showTooltip && <Tooltip/>
          }
        </div>
      </div>
    );
  }
}

export default FileInput;