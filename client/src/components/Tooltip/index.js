import React, { Component } from 'react';

import style from './index.module.css'

class OptimizationGoalForm extends Component {

  render() {
    return (
      <div className={style.tooltip}>
        <h6>The CSV should look like this!</h6>
        <table className="u-full-width">
          <thead>
          <tr>
            <th>pickupStreet*</th>
            <th>pickupZip</th>
            <th>pickupCity*</th>
            <th>pickupCountry*</th>
            <th>deliveryStreet*</th>
            <th>deliveryZip</th>
            <th>deliveryCity*</th>
            <th>deliveryCountry*</th>
            <th>pickupTimeFrom</th>
            <th>pickupTimeTo</th>
            <th>deliveryTimeFrom</th>
            <th>deliveryTimeTo</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>Weberstr. 21</td>
            <td>8004</td>
            <td>Zürich</td>
            <td>Schweiz</td>
            <td>Neue Allmendstr. 10</td>
            <td>8703</td>
            <td>Erlenbach</td>
            <td>Schweiz</td>
            <td>2018-09-05T6:00:20.345Z</td>
            <td>2018-09-05T18:00:20.345Z</td>
            <td>2018-09-05T6:00:20.345Z</td>
            <td>2018-09-05T18:00:20.345Z</td>
          </tr>
          </tbody>
        </table>
        *required fields <br/>
      </div>
    );
  }
}

export default OptimizationGoalForm