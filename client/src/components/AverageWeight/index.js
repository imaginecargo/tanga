import React from 'react';
import style from './index.module.css'

export class AverageWeight extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      averageWeight: null,
      averageLength: null,
      averageHeight: null,
      averageWidth: null,
    };

    this.handleChange = this.handleChange.bind(this)
  };

  handleChange(event){
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    }, () => {
      this.props.sendData(this.state)
    });
  };

  render(){
    return(
    <div className={style.root}>
      <div className="container u-full-width u-max-full-width">
        <h4>Durchschnittliches Gewicht und Volumen</h4>
        <div className="container u-full-width u-max-full-width">
          <div className="row">
            <div className="twelve columns">
              <label className={style.label}>
                Durchschnittliches Gewicht:
                <input type="number" name="averageWeight"  className={style.inputWeight} placeholder={"Gewicht in kg"} onChange={this.handleChange}/>
              </label>
            </div>
          </div>
          <div className="row">
            <div className="one-third column">
              <label className={style.label}>
                Durchschnittliche Tiefe:
                <input type="number" name="averageLength" className={style.input} placeholder={"Tiefe in cm"} onChange={this.handleChange}/>
              </label>
            </div>
            <div className="one-third column">
              <label className={style.label}>
                Durchschnittliche Breite:
                <input type="number" name="averageWidth" className={style.input} placeholder={"Breite in cm"} onChange={this.handleChange}/>
              </label>
            </div>
            <div className="one-third column">
              <label className={style.label}>
                Durchschnittliche Höhe:
                <input type="number" name="averageHeight" className={style.input} placeholder={"Höhe in cm"} onChange={this.handleChange}/>
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
    )
  }
}

export default AverageWeight;