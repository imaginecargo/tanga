import React from 'react';
import style from './index.module.css'

export class AverageDuration extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      averagePickupDuration: null,
      averageDeliveryDuration: null
    };

    this.handleChange = this.handleChange.bind(this)
  };


  handleChange(event){
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    }, () => {
      this.props.sendData(this.state)
      });
  };

  render() {

    return (
      <div className={style.root}>
        <div className="container u-full-width u-max-full-width">
          <h4>Durchschnittliche Dauer der Stopps</h4>
          <p> Wenn Sie die durchschnittliche Dauer eines Stopps setzten, wird diese auf alle Stopps angewendet. Die Dauer eines einzelnen Stopps wird dann nicht mehr berücksichtigt.</p>
          <div className="container u-full-width u-max-full-width">
            <div className="row">
              <div className="six columns">
                <label className={style.label}>
                  Durchschnittliche Dauer der Abholung:
                  <input type="number" name="averagePickupDuration"  className={style.duration} placeholder={"Dauer der Abholung in Minuten"} onChange={this.handleChange}/>
                </label>
              </div>
              <div className="six columns">
                <label className={style.label}>
                  Durchschnittliche Dauer der Lieferung:
                  <input type="number" name="averageDeliveryDuration" className={style.duration} placeholder={"Dauer der Lieferung in Minuten"} onChange={this.handleChange}/>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}

export default AverageDuration