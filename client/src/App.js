import React from 'react';
import axios from 'axios';
import './App.css';
import './skeleton.css';
import './normalize.css';

import { Container, Menu, Image, Dropdown } from 'semantic-ui-react';

// components
import VehicleForm from './components/VehicleForm'
import OptimizationGoalForm from "./components/OptimizationGoalForm";
import Map from './components/Map'
import CreateStopps from './components/CreateStopps';
import AverageDuration from './components/AverageDuration';
import AverageWeight from './components/AverageWeight';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state =  {
      vehicle_types: {},
      objectives: [],
      stopps: {},
      geoJSON: null,
      averageDuration:
        {
          averagePickupDuration: null,
          averageDeliveryDuration: null
        },
      averageWeight:
        {
        averageWeight: null,
        averageLength: null,
        averageHeight: null,
        averageWidth: null,
        }
    };
    this.getVehicleTypes = this.getVehicleTypes.bind(this);
    this.getOptimizationGoal = this.getOptimizationGoal.bind(this);
    this.getStopps = this.getStopps.bind(this);
    this.createSettings = this.createSettings.bind(this);
    this.getAverageDuration = this.getAverageDuration.bind(this);
    this.getAverageWeight = this.getAverageWeight.bind(this);
    this.handler = this.handler.bind(this);
  }

  handler() {
    this.setState({
      geoJSON: null
    })
  }


  getVehicleTypes(vehicleTypes){
    this.setState({vehicle_types: vehicleTypes});
  }

  getOptimizationGoal(goal){
    this.setState({objectives: [goal]});
  }


  getStopps(stopps){
    this.setState({stopps: stopps});
  }

  getAverageDuration(duration){
    this.setState({averageDuration: duration});
  }

  getAverageWeight(weight){
    this.setState({averageWeight: weight})
  }

  createSettings() {
    this.setState({
      geoJSON: null
    });
    this.getState((curState) => {
      console.log(curState)
    });
    axios.post('/api/v1/calculateroute',
      {
        vehicleObject: this.state.vehicle_types,
        stopps: this.state.stopps,
        calculateObjective: this.state.objectives,
        averageDuration : this.state.averageDuration,
        averageWeight: this.state.averageWeight
      }).then((response) => {
      this.setState({geoJSON: response.data.geojson})

    });
  }

  getState(callback) {
    this.setState((prevState) => {
      callback(prevState);
    });
  }



  render() {
    return (
      <div className="App">
        <Menu fixed='top' inverted>
          <Container>
            <Menu.Item as='a' header>
              <Image size='mini' src='/logo.png' style={{ marginRight: '1.5em' }} />
              Project Name
            </Menu.Item>
            <Menu.Item as='a'>Home</Menu.Item>

            <Dropdown item simple text='Dropdown'>
              <Dropdown.Menu>
                <Dropdown.Item>List Item</Dropdown.Item>
                <Dropdown.Item>List Item</Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Header>Header Item</Dropdown.Header>
                <Dropdown.Item>
                  <i className='dropdown icon' />
                  <span className='text'>Submenu</span>
                  <Dropdown.Menu>
                    <Dropdown.Item>List Item</Dropdown.Item>
                    <Dropdown.Item>List Item</Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown.Item>
                <Dropdown.Item>List Item</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Container>
        </Menu>

        <Container style={{ marginTop: '7em' }}>
          <VehicleForm sendVehicleTypes={this.getVehicleTypes}/>
        </Container>

        <Container>
          <CreateStopps sendData={this.getStopps}/>
        </Container>
        <Container>
          <AverageDuration sendData={this.getAverageDuration}/>
        </Container>
        <Container>
          <AverageWeight sendData={this.getAverageWeight}/>
        </Container>
        <Container>
          <OptimizationGoalForm sendData={this.getOptimizationGoal}/>
        </Container>
        <Container>
          <button className="calculate" onClick={this.createSettings || this.handler}>Berechnung starten</button>
          <button className="calculate" onClick={this.handler}>Berechnung zurücksetzen</button>
        </Container>
          { this.state.geoJSON !== null &&
              <Container>
                <Map value={this.state.geoJSON}/>
              </Container>
          }
          { this.state.geoJSON === null &&
            <p>...</p>
          }

      </div>
    );
  }
}

export default App;
