FROM node:10.13-alpine
LABEL maintainers="Simon Schütze <simon.schuetze@imaginecargo.de>, Nicolas Roos <nicolas.roos@imaginecargo.de>"
ARG ENV="development"
ARG APP_VERSION="dev"
ENV APP_VERSION $APP_VERSION
ENV ENV $ENV

RUN mkdir /app
WORKDIR /app
ADD ./client /app/client
ADD ./server /app/server
ADD ./package.json /app/package.json
RUN cd /app/client && npm install && npm run build
RUN cd /app/server && npm install

CMD npm run server:prod