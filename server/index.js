const server = require('./src/server');
const config = require('./config');
const debug = require('debug')('server:server');
const http = require('http');

function normalizePort(val) {
  const port = parseInt(val, 10);
  if (isNaN(port)) { return val; }
  if (port >= 0) { return port; }
  return false;
}

const port = normalizePort(process.env.PORT || config.get('PORT'));
server.set('port', port);

function onError(error) {
  if (error.syscall !== 'listen') { throw error; }
  switch (error.code) {
    case 'EACCES':
      process.exit(1);
      break;
    case 'EADDRINUSE':
      process.exit(1);
      break;
    default:
      throw error;
  }
}

const run = http.createServer(server);

function onListening() {
  const addr = run.address();
  const bind = typeof addr === 'string' ? `Pipe ${port}` : `Port ${port}`;
  console.log(`Listening on ${bind}`);
  debug(`Listening on ${bind}`);
}


run.listen(port);
run.on('error', onError);
run.on('listening', onListening);
