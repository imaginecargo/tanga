const express = require('express');
const path = require('path');
const fs = require('fs');
const bodyParser = require('body-parser')
const graphhopper = require('./../indexGraphHopper');
const utils = require('./utils');

const server = express();

//server.set('views', path.join(__dirname, 'views'));
//server.set('view engine', 'pug');

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));


//CORS middleware
const allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', 'localhost:3000');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
};
if(process.env.NODE_ENV === 'development') {
  server.use(allowCrossDomain)
}

server.post('/api/v1/calculateroute', utils.asyncMiddleware(async (req, res) => {
  const { vehicleObject, stopps, calculateObjective, averageDuration, averageWeight } = req.body;
  const locations = await graphhopper.convertLocation(stopps, averageDuration, averageWeight);
  const depot = await graphhopper.getDepotLocation(vehicleObject);
  const vehicleTypes = await graphhopper.createVehicleTypes(vehicleObject);
  const vehicleArray = await graphhopper.createVehicleArray(vehicleObject, depot);
  const job = await graphhopper.callGraphHopper(calculateObjective, locations, vehicleTypes, vehicleArray);
  const geoJSON = await graphhopper.createGeoJSON(job);

  res.json({ geojson: geoJSON });
}));


server.use('/static', express.static(path.join(__dirname,'..','..','client','build','static')));
server.get('*', (req, res) => {
  const index = fs.readFileSync(path.join(__dirname, '..','..','client','build','index.html'), 'utf8');
    //.replace('%PLACEHOLDER_GOOGLE_CLIENT_ID%', config.get('GOOGLE_CLIENT_ID'));
  res.set('Content-Type', 'text/html');
  res.send(index);
});

// Basic error handler
server.use((err, req, res, next) => {
  /* jshint unused:false */
  if(err.status === 401) {
    return res.status(401).send();
  }
  console.error(err);
  // If our routes specified a specific response, then send that. Otherwise,
  // send a generic message so as not to leak anything.
  res.statusCode = 500;
  res.end(res.sentry + '\n');
});

module.exports = server;
