const asyncMiddleware = fn =>
  (req, res, next) => {
    Promise.resolve(fn(req, res, next))
      .catch(next);
  };

const asyncTimeout = function (ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
};


module.exports = {
  asyncMiddleware,
  asyncTimeout,
};
