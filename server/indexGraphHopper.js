const moment = require('moment');
const querystring = require('querystring');
const axios = require('axios');
const config = require('./config');
const data = require('./test-data/smallhopper.json');
const vehicleTypes = require('./test-data/vehicle_types');
const fs = require('fs');
const GeoJSON = require('geojson');

const GOOGLE_API_KEY = config.get('GOOGLE_API_KEY');
const GRAPHHOPPER_API_KEY = config.get('GRAPHHOPPER_API_KEY');

const stroke = ['#3366cc', '#009933', '#cc0000', '#ffff00', '#808080', '#ff6600', '#000000', '#993399', '#33cccc', '#ff66ff'];
const SHIPPING_FACTOR = 5000;
const EARLIEST_START = 28800; // 28800 are the seconds until 8am
const LATEST_END = 72000; // 72000 are the seconds until 8pm

function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


function createGeoJSON(coordinates) {
  const geoJsonWaypoints = GeoJSON.parse(coordinates, { Point: 'coordinate', LineString: 'coordinates' });
  const geoJsonWaypointsString = JSON.stringify(geoJsonWaypoints, null, 4);
  /*fs.writeFile(`route-data/GeoJSON_${Date.now()}.json`, geoJsonWaypointsString, (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
  });*/
  return geoJsonWaypoints;
}

function checkIfFinished(resultJobID, costsOfVehicles) {
  return axios.get(`https://graphhopper.com/api/1/vrp/solution/${resultJobID}?key=${GRAPHHOPPER_API_KEY}`).then((statusJob) => {
    if (statusJob.data.status !== 'finished') {
      setTimeout(() => {
        checkIfFinished(resultJobID);
      }, 600);
      return;
    }
    /*const solutionGraphhopper = statusJob.data.solution;
    const jsonFile = JSON.stringify(solutionGraphhopper, null, 4);
    fs.writeFile('route-data/SOLUTION.json', (jsonFile), (err) => {
      if (err) throw err;
      console.log('The file has been saved!');
    }); */

    const job = statusJob.data.solution.routes;
    const routes = job.map((bike, index) => {
      const coordinates = bike.points.reduce((prevValue, currentValue) => {
        if (currentValue.type !== 'Point') {
          prevValue.push(...currentValue.coordinates);
        }
        return prevValue;
      }, []);
      const costsPerHour = costsOfVehicles.find(x => x.vehicle_id === bike.vehicle_id);
      const costs = costsPerHour.costs * (bike.completion_time/3600);
      return {
        stroke: stroke[index],
        'stroke-width': 2,
        'stroke-opacity': 1,
        distance: bike.distance,
        duration: bike.completion_time,
        id: bike.vehicle_id,
        costs: costs,
        points: bike.activities.map((address) => {
          return {
            type: 'Feature',
            geometry: {
              type: 'Point',
              coordinates: [address.address.lon, address.address.lat],
            },
            properties: {
              title: address.type,
              icon: 'monument',
            },
          };
        }),
        stopps: bike.activities,
        coordinates,
      };
    });
    return routes;
  }).catch(error =>
    // console.log(error)
    false,
    // return Promise.reject(error);
  );
}

async function getGeocodingFromAddress(address) {
  const params = querystring.stringify({
    address,
    key: GOOGLE_API_KEY,
  });

  const result = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?${params}`);
  return result.data;
}

async function getDepotLocation(vehicleObject) {
  const latLng = await Promise.all(vehicleObject.map(async (vehicle) => {
    const depotCoordinates = await getGeocodingFromAddress(vehicle.options.depot.address);
    const depotLocation = depotCoordinates.results[0].geometry.location;
    return { depotLocation, depotAddress: vehicle.options.depot.address };
  }));
  return latLng.map(location => ({
    coordinates: location.depotLocation,
    name: location.depotAddress,
  }));
}

async function callGraphHopper(calculateObjective, locations, vehicleTypesArray, vehicleArray) {
  const options = {
    routing:
      { calc_points: true,
        fail_fast: false
      },
  };
  const apiCall = {
    configuration: options,
    objectives: calculateObjective,
    vehicles: vehicleArray[0],
    vehicle_types: vehicleTypesArray,
    shipments: locations,
  };
  const apiCallString = JSON.stringify(apiCall);
  const jsonFile = JSON.stringify(apiCall, null, 4);
  fs.writeFile('./testAPICALL.json', (jsonFile), (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
  });
  const resultJobID = await axios({
    url: `https://graphhopper.com/api/1/vrp/optimize?key=${GRAPHHOPPER_API_KEY}`,
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    data: apiCallString,
  });


  const prom = new Promise((async (resolve, reject) => {
    let result = null;
    while (!result) {
      const res = await Promise.all([
        checkIfFinished(resultJobID.data.job_id, vehicleArray[1]),
        timeout(1000)
      ]);
      result = res[0]
    }
    resolve(result);
  }));
  return prom;
}

function getTimeSlotFromFrontend(time) {
  const hour = moment(time).hour();
  const minute = moment(time).minute();
  const seconds = ((hour * 60) + minute) * 60;
  return seconds;
}

function getDuration(order, averageDuration){
  if(averageDuration === null){
    return Number(order);
  } else {
    return averageDuration * 60;
  }
}

function getWeight(weight, length, height, width){
  const volume = length * height * width;
  if(volume !== 0 && weight !== null){
    if((volume/SHIPPING_FACTOR) >= weight){
      return (volume/SHIPPING_FACTOR)*1000
    }
    else return weight*1000
  }
  else if(volume !== 0 && weight === null){
    return (volume/SHIPPING_FACTOR)*1000
  }
  else if(volume === 0 && weight !== null){
    return weight*1000
  }
  else return 2000
}

async function convertLocation(locations, averageDuration, averageWeight) {
  const latLngPickUp = await Promise.all(locations.map(async (order) => {
    const pickup = await getGeocodingFromAddress(`${order.pickup_street} ${order.pickup_number}, ${order.pickup_place}, ${order.pickup_country}`);
    const pickupAddress = `${order.pickup_street} ${order.pickup_number}, ${order.pickup_place}, ${order.pickup_country}`;
    const delivery = await getGeocodingFromAddress(`${order.delivery_street} ${order.delivery_number}, ${order.delivery_place}, ${order.delivery_country}`);
    const deliveryAddress = `${order.delivery_street} ${order.delivery_number}, ${order.delivery_place}, ${order.delivery_country}`;
    const pickupLocation = pickup.results[0].geometry.location;
    const deliveryLocation = delivery.results[0].geometry.location;
    const earliestPickup = await getTimeSlotFromFrontend(order.earliest_pickup);
    const latestPickup = await getTimeSlotFromFrontend(order.latest_pickup);
    const earliestDelivery = await getTimeSlotFromFrontend(order.earliest_delivery);
    const latestDelivery = await getTimeSlotFromFrontend(order.latest_delivery);
    const pickupDuration = await getDuration(order.pickup_duration, averageDuration.averagePickupDuration);
    const deliveryDuration = await getDuration(order.delivery_duration, averageDuration.averageDeliveryDuration);
    const weight = await getWeight(averageWeight.averageWeight, averageWeight.averageLength, averageWeight.averageHeight, averageWeight.averageWidth);

    return {
      pickupLocation,
      deliveryLocation,
      pickupAddress,
      deliveryAddress,
      earliestPickup,
      latestPickup,
      earliestDelivery,
      latestDelivery,
      pickupDuration,
      deliveryDuration,
      weight
    };
  }));
  const arrayLatLng = latLngPickUp.map((location, index) => ({
    id: `shipment_${index + 1}`,
    name: '',
    pickup: {
      address: {
        location_id: `locationPickUp_${index + 1}`,
        lon: location.pickupLocation.lng,
        lat: location.pickupLocation.lat,
        name: location.pickupAddress,
      },
      duration: location.pickupDuration,
      time_windows: [
        {
          earliest: location.earliestPickup,
          latest: location.latestPickup,
        },
      ],
      preparation_time: 0,
    },
    delivery: {
      address: {
        location_id: `locationDelivery_${index + 1}`,
        lon: location.deliveryLocation.lng,
        lat: location.deliveryLocation.lat,
        name: location.deliveryAddress,
      },
      duration: location.deliveryDuration,
      time_windows: [
        {
          earliest: location.earliestDelivery,
          latest: location.latestDelivery,
        },
      ],
      preparation_time: 0,
    },
    size: [location.weight],
  }));
  return arrayLatLng;
}

async function createVehicleTypes(vehicleObjects) {
  const vehicleTypesArray = vehicleObjects.map(vehicleType => ({
    type_id: `vehicleType_${vehicleType.type_id}`,
    profile: vehicleType.profile,
    capacity: vehicleType.capacity,
  }));
  return vehicleTypesArray;
}

async function createVehicleArray(vehicleArray, depot) {
  const vehicleList = [];
  const costsOfVehicleList = [];
  vehicleArray.map((vehicle, index) => {
    for (let i = 0; i < vehicle.options.number; i++) {
      const vehicleObeject = {
        vehicle_id: `vehicle_${index + 1}_${i + 1}`,
        start_address: {
          location_id: `location_${index + 1}`,
          lon: depot[vehicle.type_id - 1].coordinates.lng,
          lat: depot[vehicle.type_id - 1].coordinates.lat,
          name: depot[vehicle.type_id - 1].name,
        },
        type_id: `vehicleType_${vehicle.type_id}`,
        earliest_start: vehicle.earliest_start ? getTimeSlotFromFrontend(vehicle.earliest_start) : EARLIEST_START,
        latest_end: vehicle.latest_end ? getTimeSlotFromFrontend(vehicle.latest_end) : LATEST_END,
      };
      const costsOfVehicle ={
        vehicle_id: `vehicle_${index + 1}_${i + 1}`,
        costs: vehicle.options.costs
      }

      vehicleList.push(vehicleObeject);
      costsOfVehicleList.push(costsOfVehicle);
    }
  });
  return [vehicleList,costsOfVehicleList];
}


module.exports = {
  createGeoJSON,
  convertLocation,
  getDepotLocation,
  createVehicleTypes,
  createVehicleArray,
  callGraphHopper,
};

